package com.bitbucket.mailSender;

import com.bitbucket.mailSender.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.internet.MimeMessage;

@SpringBootApplication
public class MailSenderApplication implements CommandLineRunner {

	@Autowired
	@Qualifier("mainMailSender")
	JavaMailSender emailSender;

	@Autowired
	Environment environment;

	@Autowired
	TestService testService;


	public static void main(String[] args) {
		SpringApplication.run(MailSenderApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		String textEmailResult = "Тестовое письмо";

		testService.dummyService();
		MimeMessage mimeMessage = emailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
		helper.setFrom(environment.getProperty("spring.mail.username"));
		helper.setTo(new String[] {"koks-gops@ya.ru","dimich14@gmail.com"});
		//helper.setTo("koks-gops@ya.ru");
		helper.setText(textEmailResult);
		helper.setSubject("Тестовое письмо для проверки Bitbucket и teamCity");
		emailSender.send(mimeMessage);
	}
}
